%option noyywrap
%option noinput
%option nounput
%option yylineno

%{
        #include "logika.h"
        #include "parser.h"
        #include <iostream>
%}

%option header-file="lexer.h"
%option outfile="lexer.cpp"


%%

T   				      return TRUE;
F				      return FALSE;
[a-z][a-zA-Z_0-9]*                    yylval.str_attr = new string(yytext); return SYMBOL;
[A-Za-z][A-Za-z_0-9]*		      yylval.str_attr = new std::string(yytext); return VARIABLE;
\(				      return *yytext;
\)				      return *yytext;
\/\\                                  return AND;
\\\/                                  return OR;
=\>                                   return IMP;
\<=\>                                 return IFF;
\~				      return NOT;
;				      return *yytext;
[ \t\n]

%%
