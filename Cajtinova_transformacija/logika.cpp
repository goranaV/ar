#include "logika.h"

static uint64_t s_UniqueCounter = 1U;

Valuation::Valuation(const AtomSet &aset)
{
    reset(aset);
}

void Valuation::reset(const AtomSet &aset)
{
    m_map.clear();
    for(string atom : aset)
        m_map[atom] = false;
}

void Valuation::setAtom(string atom, bool val)
{
    m_map[atom] = val;
}


bool Valuation::getAtom(string atom) const
{
    auto it = m_map.find(atom);
    if(it != m_map.cend())
        return it->second;
    throw std::runtime_error{"Atom not found!"};
}

bool Valuation::next()
{
    auto first = m_map.rbegin();
    auto last = m_map.rend();

    for( ; first != last; ++first){
        first->second = !first->second;
        if(first->second)
            break;
    }
    return first != last;
}

ostream &Valuation::print(ostream &out) const
{

    out << std::noboolalpha;
    for(const auto& key : m_map)
        out << key.second << ' ';
    return out;
}

BaseFormula::~BaseFormula()
{

}

void BaseFormula::getAtoms(AtomSet &aset) const
{
    ((void)aset);
}

bool BaseFormula::equalTo(const Formula &f) const
{
    const BaseFormula *pF = f.get();
    return typeid(*pF) == typeid(*this);
}

LiteralListList BaseFormula::cross(const LiteralListList &l1, const LiteralListList &l2)
{
    LiteralListList result;
    result.reserve(l1.size() * l2.size());

    for(const auto &cl1 : l1){
       for(const auto &cl2 : l2)
           result.push_back(concatenate(cl1, cl2));
    }
    return result;
}

template<typename T>
T BaseFormula::concatenate(const T &l1, const T &l2)
{
    T result;
    result.reserve(l1.size() + l2.size());
    //kopiramo iz prve liste elemente i dodajemo na kraj rezultujuce
    std::copy(l1.cbegin(), l1.cend(), std::back_inserter(result));
    std::copy(l2.cbegin(), l2.cend(), std::back_inserter(result));
    return result;
}

LiteralListList BaseFormula::listCNF() const
{
    throw std::runtime_error{"Logic connective not supported"};
}

Formula BaseFormula::transform(AtomSet& aset, const Formula &formula, Formula& tmp_formula){
    if(BaseFormula::isOfType<True>(formula) || BaseFormula::isOfType<False>(formula)){
        return formula;
    }
    else if(BaseFormula::isOfType<Atom>(formula)){
        return formula;
    }
    else if(BaseFormula::isOfType<Not>(formula)){
        return formula;
    }
    else{
        const BinaryConnective* f_bc = static_cast<const BinaryConnective*>(formula.get());
        Formula f_op1, f_op2;
        std::tie(f_op1, f_op2) = f_bc->getOperands();
        Formula f1 = transform(aset, f_op1, tmp_formula);
        Formula f2 = transform(aset, f_op2, tmp_formula);

        string new_atom = getUniqueAtomId(aset);
        Formula atom = make_shared<Atom>(new_atom);
        aset.insert(new_atom);

        Formula f_c;
        if(BaseFormula::isOfType<And>(formula)){
            f_c = make_shared<And>(f1, f2);
        }
        else if(BaseFormula::isOfType<Or>(formula)){
            f_c = make_shared<Or>(f1,f2);
        }
        else if(BaseFormula::isOfType<Imp>(formula)){
            f_c = make_shared<Imp>(f1,f2);
        }
        else if(BaseFormula::isOfType<Iff>(formula)){
            f_c = make_shared<Iff>(f1,f2);
        }

        if(tmp_formula.get() == nullptr){
            tmp_formula  = make_shared<Iff>(atom, f_c);
        }else{
            tmp_formula = make_shared<And>(tmp_formula, make_shared<Iff>(atom, f_c));
        }

        return atom;
    }
}


AtomicFormula::AtomicFormula() : BaseFormula()
{

}

Formula AtomicFormula::simplify() const
{
    return std::const_pointer_cast<BaseFormula>(shared_from_this());
}

Formula AtomicFormula::nnf() const
{
    return std::const_pointer_cast<BaseFormula>(shared_from_this());
}

Formula AtomicFormula::notSimpl() const
{
    return std::const_pointer_cast<BaseFormula>(shared_from_this());
}


True::True() : AtomicFormula()
{

}

ostream &True::print(ostream &out) const
{
    return out << "T";
}

bool True::eval(Valuation &val) const
{
    ((void)val);
    return true;
}

QString& True::print(QString &s) const
{
   s.append("T");
   return s;
}

False::False() : AtomicFormula()
{

}

ostream &False::print(ostream &out) const
{
    return out << "F";
}

bool False::eval(Valuation &val) const
{
    ((void)val);
    return false;
}

QString &False::print(QString &s) const
{
    s.append("F");
    return s;
}

Atom::Atom(const string id) : AtomicFormula(), m_id(id)
{

}

ostream &Atom::print(ostream &out) const
{

    return out << m_id;
}

void Atom::getAtoms(AtomSet &aset) const
{
    aset.insert(m_id);
}

bool Atom::eval(Valuation &val) const
{
    return val.getAtom(m_id);
}

bool Atom::equalTo(const Formula &f) const
{
    if(BaseFormula::equalTo(f)){
        return static_cast<const Atom*>(f.get())->m_id == m_id;

    }
    else{
        return false;
    }
}

QString &Atom::print(QString &s) const
{
    std::string str = m_id;
    QString qstr = QString::fromStdString(str);
    s.append(qstr);
    return s;
}


LiteralListList Atom::listCNF() const
{
   return {{std::const_pointer_cast<BaseFormula>(shared_from_this())}};
}

UnaryConnective::UnaryConnective(const Formula &f) : BaseFormula(), m_op(f)
{

}

void UnaryConnective::getAtoms(AtomSet &aset) const
{
    m_op->getAtoms(aset);
}

bool UnaryConnective::equalTo(const Formula &f) const
{
    if(BaseFormula::equalTo(f)){
        return m_op->equalTo(static_cast<const UnaryConnective*>(f.get())->m_op);
    }
    return false;
}

Not::Not(const Formula &f) : UnaryConnective(f)
{

}

bool Not::eval(Valuation &val) const
{
    return !m_op->eval(val);
}

ostream &Not::print(ostream &out) const
{
    out << "~(";
    m_op->print(out);
    return out << ")";
}

QString &Not::print(QString &s) const
{
    if(BaseFormula::isOfType<True>(m_op) || BaseFormula::isOfType<False>(m_op) || BaseFormula::isOfType<Atom>(m_op) || BaseFormula::isOfType<Not>(m_op)){
        s.append("~");
        m_op->print(s);
    }
    else{
        s.append("~(");
        m_op->print(s);
        s.append(")");

    }
     return s;
}

Formula Not::simplify() const
{
    Formula simplifiedOp = m_op->simplify();
    if(BaseFormula::isOfType<True>(m_op))
        return std::make_shared<False>();
    else if(BaseFormula::isOfType<False>(m_op))
        return std::make_shared<True>();
    else
        return std::make_shared<Not>(simplifiedOp);
}
Formula Not::nnf() const
{
    if(BaseFormula::isOfType<Not>(m_op)){
        const Not* pNot = static_cast<const Not*>(m_op.get());
        return pNot->m_op->nnf();

    }
    else if(BaseFormula::isOfType<And>(m_op)){
        const And *pAnd = static_cast<const And*>(m_op.get());
        Formula op1, op2;
        std::tie(op1, op2) = pAnd->getOperands();
        return std::make_shared<Or>(std::make_shared<Not>(op1)->nnf(), std::make_shared<Not>(op2)->nnf());
    }
    else if(BaseFormula::isOfType<Or>(m_op)){
        const Or* pOr = static_cast<const Or*>(m_op.get());
        Formula op1, op2;
        std::tie(op1, op2) = pOr->getOperands();
        return std::make_shared<And>(std::make_shared<Not>(op1)->nnf(), std::make_shared<Not>(op2)->nnf());
    }
    else if(BaseFormula::isOfType<Imp>(m_op)){
        const Imp* pImp = static_cast<const Imp*>(m_op.get());
        Formula op1, op2;
        std::tie(op1, op2) = pImp->getOperands();
        return std::make_shared<And>(op1->nnf(), std::make_shared<Not>(op2)->nnf());
    }
    else if(BaseFormula::isOfType<Iff>(m_op)){
        const Iff* pIff = static_cast<const Iff*>(m_op.get());
        Formula op1, op2;
        std::tie(op1, op2) = pIff->getOperands();
        return std::make_shared<Or>(std::make_shared<And>(op1->nnf(), std::make_shared<Not>(op2)->nnf()),
                                    std::make_shared<And>(std::make_shared<Not>(op1)->nnf(), op2->nnf()));
    }
    return std::const_pointer_cast<BaseFormula>(shared_from_this());
}

Formula Not::notSimpl() const
{
    if(BaseFormula::isOfType<Not>(m_op)){
        return static_cast<const Not*>(m_op.get())->m_op->notSimpl();
    }
    else if(BaseFormula::isOfType<And>(m_op)){
        const And* pAnd = static_cast<const And*>(m_op.get());
        Formula op1, op2;
        std::tie(op1, op2) = pAnd->getOperands();
        return std::make_shared<Or>(make_shared<Not>(op1)->notSimpl(), make_shared<Not>(op2)->notSimpl());
    }
    else if(BaseFormula::isOfType<Or>(m_op)){
        const Or* pOr = static_cast<const Or*>(m_op.get());
        Formula op1, op2;
        std::tie(op1, op2) = pOr->getOperands();
        return std::make_shared<And>(make_shared<Not>(op1)->notSimpl(), make_shared<Not>(op2)->notSimpl());
    }
    else if(BaseFormula::isOfType<Imp>(m_op)){
        const Imp* pImp = static_cast<const Imp*>(m_op.get());
        Formula op1, op2;
        std::tie(op1, op2) = pImp->getOperands();
        return std::make_shared<And>(op1->notSimpl(), make_shared<Not>(op2)->notSimpl());

    }
    else if(BaseFormula::isOfType<Iff>(m_op)){
        const Iff* pIff = static_cast<const Iff*>(m_op.get());
        Formula op1, op2;
        std::tie(op1, op2) = pIff->getOperands();
        return std::make_shared<Iff>(make_shared<Not>(op1)->notSimpl(), op2->notSimpl());

    }
    else{
        return std::const_pointer_cast<BaseFormula>(shared_from_this());
    }
}

LiteralListList Not::listCNF() const
{
    return {{std::const_pointer_cast<BaseFormula>(shared_from_this())}}; // lista koja sadrzi taj literal

}

BinaryConnective::BinaryConnective(const Formula &op1, const Formula &op2) : BaseFormula(), m_op1(op1), m_op2(op2)
{

}

ostream &BinaryConnective::print(ostream &out) const
{
    out << "(";
    m_op1->print(out);
    out << " " << symbol() << " ";
    m_op2->print(out);
    return out << ")";
}

void BinaryConnective::getAtoms(AtomSet &aset) const
{
    m_op1->getAtoms(aset);
    m_op2->getAtoms(aset);
}

bool BinaryConnective::equalTo(const Formula &f) const
{
    if(BaseFormula::equalTo(f))
        return m_op1->equalTo(static_cast<const BinaryConnective*>(f.get())->m_op1) &&
                m_op2->equalTo(static_cast<const BinaryConnective*>(f.get())->m_op2);
    return false;
}


QString &BinaryConnective::print(QString &s) const
{
    QString str = QString::fromStdString(symbol());

    if(BaseFormula::isOfType<True>(m_op1) || BaseFormula::isOfType<False>(m_op1) || BaseFormula::isOfType<Atom>(m_op1) || BaseFormula::isOfType<Not>(m_op1)){
        m_op1->print(s);
    }
    else{
        s.append("(");
        m_op1->print(s);
        s.append(")");
    }

    s.append(" ");
    s.append(str);
    s.append(" ");

    if(BaseFormula::isOfType<True>(m_op2) || BaseFormula::isOfType<False>(m_op2) || BaseFormula::isOfType<Atom>(m_op2) || BaseFormula::isOfType<Not>(m_op2)){
        m_op2->print(s);
    }
    else{
        s.append("(");
        m_op2->print(s);
        s.append(")");
    }

    return s;

}

And::And(const Formula &op1, const Formula &op2) : BinaryConnective(op1, op2)
{

}

bool And::eval(Valuation &val) const
{
    return m_op1->eval(val) && m_op2->eval(val);
}

std::__cxx11::string And::symbol() const
{
    return "/\\";
}


Formula And::simplify() const
{
    Formula simplifiedOp1 = m_op1->simplify();
    Formula simplifiedOp2 = m_op2->simplify();
    if(BaseFormula::isOfType<True>(simplifiedOp1))
        return simplifiedOp2;
    else if(BaseFormula::isOfType<True>(simplifiedOp2))
        return simplifiedOp1;
    else if(BaseFormula::isOfType<False>(simplifiedOp1))
        return std::make_shared<False>();
    else if(BaseFormula::isOfType<False>(simplifiedOp2))
        return std::make_shared<False>();
    else
        return std::make_shared<And>(simplifiedOp1, simplifiedOp2);
}

Formula And::nnf() const
{
    return std::make_shared<And>(m_op1->nnf(), m_op2->nnf());
}

Formula And::notSimpl() const
{
    return std::make_shared<And>(m_op1->notSimpl(), m_op2->notSimpl());
}

LiteralListList And::listCNF() const
{
    return BaseFormula::concatenate(m_op1->listCNF(), m_op2->listCNF());
}

Or::Or(const Formula &op1, const Formula &op2) : BinaryConnective(op1, op2)
{

}

bool Or::eval(Valuation &val) const
{
    return m_op1->eval(val) || m_op2->eval(val);
}

std::__cxx11::string Or::symbol() const
{
    return "\\/";
}

Formula Or::simplify() const
{
    Formula simplifiedOp1 = m_op1->simplify();
    Formula simplifiedOp2 = m_op2->simplify();
    if(BaseFormula::isOfType<True>(simplifiedOp1))
       return std::make_shared<True>();
     else if(BaseFormula::isOfType<True>(simplifiedOp2))
       return std::make_shared<True>();
    else if(BaseFormula::isOfType<False>(simplifiedOp1))
       return simplifiedOp2;
    else if(BaseFormula::isOfType<False>(simplifiedOp2))
       return simplifiedOp1;
    else
      return std::make_shared<Or>(simplifiedOp1, simplifiedOp2);
}

Formula Or::nnf() const
{
    return std::make_shared<Or>(m_op1->nnf(),m_op2->nnf());
}

Formula Or::notSimpl() const
{
     return std::make_shared<Or>(m_op1->notSimpl(), m_op2->notSimpl());
}

LiteralListList Or::listCNF() const
{
    return BaseFormula::cross(m_op1->listCNF(), m_op2->listCNF());

}

Imp::Imp(const Formula &op1, const Formula &op2) : BinaryConnective(op1, op2)
{

}

bool Imp::eval(Valuation &val) const
{
    return !m_op1->eval(val) || m_op2->eval(val);
}

std::__cxx11::string Imp::symbol() const
{
    return "=>";
}

Formula Imp::simplify() const
{
    Formula simplifiedOp1 = m_op1->simplify();
    Formula simplifiedOp2 = m_op2->simplify();
    if(BaseFormula::isOfType<True>(simplifiedOp1))
      return simplifiedOp2;
    else if(BaseFormula::isOfType<True>(simplifiedOp2))
      return std::make_shared<True>();
    else if(BaseFormula::isOfType<False>(simplifiedOp1))
      return std::make_shared<True>();
    else if(BaseFormula::isOfType<False>(simplifiedOp2))
      return std::make_shared<Not>(simplifiedOp1);
    else
      return std::make_shared<Imp>(simplifiedOp1, simplifiedOp2);
}

Formula Imp::nnf() const
{
    return std::make_shared<Or>(std::make_shared<Not>(m_op1)->nnf(),m_op2->nnf());
}

Formula Imp::notSimpl() const
{
     return std::make_shared<Or>(std::make_shared<Not>(m_op1)->notSimpl(), m_op2->notSimpl());
}

Iff::Iff(const Formula &op1, const Formula &op2) : BinaryConnective(op1, op2)
{

}

bool Iff::eval(Valuation &val) const
{
    return m_op1->eval(val) == m_op2->eval(val);
}

std::__cxx11::string Iff::symbol() const
{
   return "<=>";
}

Formula Iff::simplify() const
{
    Formula simplifiedOp1 = m_op1->simplify();
    Formula simplifiedOp2 = m_op2->simplify();
    if(BaseFormula::isOfType<True>(simplifiedOp1))
       return simplifiedOp2;
    else if(BaseFormula::isOfType<True>(simplifiedOp2))
       return simplifiedOp1;
    else if(BaseFormula::isOfType<False>(simplifiedOp1))
       return std::make_shared<Not>(simplifiedOp2)->simplify();
    else if(BaseFormula::isOfType<False>(simplifiedOp2))
       return std::make_shared<Not>(simplifiedOp1)->simplify();
    else
       return std::make_shared<Iff>(simplifiedOp1, simplifiedOp2);
}

Formula Iff::nnf() const
{
    return std::make_shared<And>(std::make_shared<Or>(std::make_shared<Not>(m_op1)->nnf(),m_op2->nnf()),
                                 std::make_shared<Or>(m_op1->nnf(),std::make_shared<Not>(m_op2)->nnf()));
}

Formula Iff::notSimpl() const
{
     return std::make_shared<Iff>(m_op1->notSimpl(), m_op2->notSimpl());
}


template<typename Derived>
bool BaseFormula::isOfType(const Formula &f)
{
    return nullptr != dynamic_cast<const Derived*>(f.get());
}

ostream &operator<<(ostream &out, const Formula &f)
{
    return f->print(out);
}

ostream &operator<<(ostream &out, const Valuation &v)
{
    return v.print(out);
}

QString &operator <<(QString &s, const Formula &f)
{
    f->print(s);
    return s;
}

string getUniqueAtomId(const AtomSet& aset){

    string name;
    do {
        name = "s" + std::to_string(s_UniqueCounter++);
    } while (aset.cend() != aset.find(name));

    return name;
}


QString &operator <<(QString &s, const LiteralListList &l)
{
    s.append("[ ");
    for(auto & ll : l){
        s.append("[ ");
        for(auto first = ll.cbegin(), last = ll.cend(); first + 1 != last; ++first){
            s << (*first);
            s.append(", ");
        }
        s << ll.back();
        s.append(" ] ");
    }
    s.append("]");
    s.append(";");
    return s;
}

