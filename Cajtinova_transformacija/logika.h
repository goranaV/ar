#ifndef LOGIKA_H
#define LOGIKA_H

#include <iostream>
#include <memory>
#include <string>
#include <set>
#include <map>
#include <typeinfo>
#include <stdexcept>
#include <vector>
#include <QString>

using namespace std;

class BaseFormula;
using Formula = std::shared_ptr<BaseFormula>;

using AtomSet = std::set<string>;
using LiteralList = std::vector<Formula>;
using LiteralListList = std::vector<LiteralList>;
extern Formula m_formula;

class Valuation{

public:
    Valuation(const AtomSet& aset);
    void reset(const AtomSet& aset);
    void setAtom(string atom, bool val);
    bool getAtom(string atom) const;
    bool next();
    std::ostream& print(std::ostream& out) const;
private:
    std::map<string, bool> m_map;
};

class BaseFormula : public std::enable_shared_from_this<BaseFormula>{
public:
    virtual ~BaseFormula();
    virtual std::ostream& print(std::ostream& out) const = 0;
    virtual void getAtoms(AtomSet& aset) const;
    virtual bool eval(Valuation &val) const = 0;
    virtual bool equalTo(const Formula& f) const;
    virtual QString& print(QString&) const = 0;

    virtual Formula simplify() const = 0;
    virtual Formula nnf() const = 0;
    virtual Formula notSimpl() const = 0;


    template<typename Derived>
    static bool isOfType(const Formula& f);

    static LiteralListList cross(const LiteralListList &l1, const LiteralListList &l2);
    template<typename T>
    static T concatenate(const T &l1, const T &l2);

    virtual LiteralListList listCNF() const;

    Formula transform(AtomSet& aset, const Formula &formula, Formula& tmp_formula);
};


class AtomicFormula : public BaseFormula{
public:
    AtomicFormula();
    virtual Formula simplify() const;
    virtual Formula nnf() const;
    virtual Formula notSimpl() const;

};

class True : public AtomicFormula{
public:
    True();
    virtual std::ostream& print(ostream &out) const;
    virtual bool eval(Valuation &val) const;
    virtual QString& print(QString&) const;
};

class False : public AtomicFormula{
public:
    False();
    virtual std::ostream& print(ostream &out) const;
    virtual bool eval(Valuation &val) const;
    virtual QString& print(QString&) const;
};


class Atom : public AtomicFormula{
public:
    Atom(const string id);
    virtual std::ostream& print(std::ostream& out) const;
    virtual void getAtoms(AtomSet &aset) const;
    virtual bool eval(Valuation &val) const;
    virtual bool equalTo(const Formula &f) const;
    virtual QString& print(QString&) const;

    virtual LiteralListList listCNF() const;
private:
    string m_id;
};

class UnaryConnective : public BaseFormula{
public:
    UnaryConnective(const Formula& f);
    virtual void getAtoms(AtomSet &aset) const;
    virtual bool equalTo(const Formula &f) const;
protected:
    Formula m_op;
};

class Not : public UnaryConnective{
public:
    Not(const Formula& f);
    virtual bool eval(Valuation &val) const;
    virtual std::ostream& print(std::ostream& out) const;
    virtual QString& print(QString&) const;
    virtual Formula simplify() const;
    virtual Formula nnf() const;
    virtual Formula notSimpl() const;


    virtual LiteralListList listCNF() const;
};

class BinaryConnective : public BaseFormula{
public:
    BinaryConnective(const Formula& op1, const Formula& op2);
    virtual std::ostream& print(std::ostream& out) const;
    virtual void getAtoms(AtomSet &aset) const;
    virtual bool equalTo(const Formula &f) const;
    virtual std::string symbol() const = 0;
    inline std::pair<Formula, Formula> getOperands() const {return {m_op1, m_op2};}
    virtual QString& print(QString&) const;

protected:
    Formula m_op1;
    Formula m_op2;
};

class And : public BinaryConnective{
public:
    And(const Formula& op1, const Formula & op2);
    virtual bool eval(Valuation &val) const;
    virtual std::string symbol() const;

    virtual Formula simplify() const;
    virtual Formula nnf() const;
    virtual Formula notSimpl() const;

    virtual LiteralListList listCNF() const;
};

class Or : public BinaryConnective{
public:
    Or(const Formula& op1, const Formula & op2);
    virtual bool eval(Valuation &val) const;
    virtual std::string symbol() const;

    virtual Formula simplify() const;
    virtual Formula nnf() const;
    virtual Formula notSimpl() const;

    virtual LiteralListList listCNF() const;
};


class Imp : public BinaryConnective{
public:
    Imp(const Formula& op1, const Formula& op2);
    virtual bool eval(Valuation &val) const;
    virtual std::string symbol() const;    

    virtual Formula simplify() const;
    virtual Formula nnf() const;
    virtual Formula notSimpl() const;

};

class Iff : public BinaryConnective{
public:
    Iff(const Formula& op1, const Formula& op2);
    virtual bool eval(Valuation &val) const;
    virtual std::string symbol() const;

    virtual Formula simplify() const;
    virtual Formula nnf() const;
    virtual Formula notSimpl() const;

};

string getUniqueAtomId(const AtomSet& aset);

ostream& operator<<(ostream &out, const Formula &f);
ostream& operator<<(ostream &out, const Valuation &v);
QString& operator <<(QString& s, const Formula& f);
QString& operator <<(QString& s, const LiteralListList& l);

#endif // LOGIKA_H
