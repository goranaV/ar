#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logika.h"
#include "parser.h"
#include "lexer.h"
#include <QFile>
#include <QTextStream>
#include <QTextEdit>

extern void yyrestart(FILE* archive);
extern int yyparse();
extern Formula formula;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pokreni_clicked()
{
   QFile file("temp.txt");
   if(file.open(file.WriteOnly)){
       QTextStream stream1(&file);
       stream1 << ui->ulaz->toPlainText();
   }

   const char *x = "temp.txt";
   FILE* input = fopen(x, "r");
   yyrestart(input);

   yyparse();

   Formula ulaz = formula;
   Formula cajtin;

   AtomSet aset;
   Formula ulaz_simpl = ulaz->simplify();
   Formula f = ulaz_simpl->notSimpl();
   f->getAtoms(aset);
   Formula f_t = nullptr;
   Formula f_r = f->transform(aset, f, f_t);
   if(f_t.get() == nullptr){
       cajtin = f_r;
   }else{
       cajtin = make_shared<And>(f_r, f_t);
   }

   QString s1;
   QTextStream ts_izlaz(&s1);
   s1 << cajtin;
   s1.append(";");
   ui->izlaz->setPlainText(s1);
   LiteralListList cnf_format = cajtin->nnf()->listCNF();

   QString s2;
   QTextStream ts2(&s2);
   s2 << cnf_format;
   ui->izlaz_cnf->setPlainText(s2);

}

